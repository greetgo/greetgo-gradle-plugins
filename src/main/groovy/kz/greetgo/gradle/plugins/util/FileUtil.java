package kz.greetgo.gradle.plugins.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

public class FileUtil {

  public static final Path UP_DIR = Paths.get("..");

  public static boolean isParent(Path parent, File file) {
    return !parent.relativize(file.toPath()).startsWith(UP_DIR);
  }

  public static File copyToDir(File file, Path dir) {
    File destination = dir.resolve(file.getName()).toFile();
    destination.getParentFile().mkdirs();
    try {
      Files.copy(file.toPath(), destination.toPath(), StandardCopyOption.COPY_ATTRIBUTES);
      return destination;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void removeFileOrDir(Path fileOrDir) {
    try {
      if (!Files.exists(fileOrDir)) {
        return;
      }

      if (Files.isDirectory(fileOrDir)) {
        try (Stream<Path> list = Files.list(fileOrDir)) {
          list.forEach(FileUtil::removeFileOrDir);
        }
      }

      Files.delete(fileOrDir);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static @Nonnull Map<String, String> configFileToMap(Path configFile) {
    try {
      final List<String>        lines = Files.readAllLines(configFile, StandardCharsets.UTF_8);
      final Map<String, String> ret   = new HashMap<>();

      for (final String line : lines) {
        String trimmedLine = line.trim();
        if (trimmedLine.startsWith("#")) {
          continue;
        }

        int idx = line.indexOf('=');
        if (idx < 0) {
          continue;
        }

        String key   = line.substring(0, idx).trim();
        String value = line.substring(idx + 1).trim();
        ret.put(key, value);
      }

      return ret;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
