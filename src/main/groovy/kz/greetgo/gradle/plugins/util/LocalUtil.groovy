package kz.greetgo.gradle.plugins.util

import org.gradle.api.Project
import org.gradle.api.tasks.bundling.Jar

class LocalUtil {

  static void registerSourcesJavadocJarTasks(Project project) {
    project.pluginManager.apply("java")

    def sourcesJar = project.tasks.findByName("sourcesJar")
    if (sourcesJar == null) {
      sourcesJar = project.tasks.create("sourcesJar", Jar.class) {
        group = "documentation"
        from project.sourceSets.main.allSource
        archiveClassifier = 'sources'
      }
    }

    def javadocJar = project.tasks.findByName("javadocJar")
    if (javadocJar == null) {
      javadocJar = project.tasks.create("javadocJar", Jar.class) {
        group = "documentation"
        from project.tasks.javadoc
        archiveClassifier = 'javadoc'
      }
    }

    project.artifacts {
      archives javadocJar, sourcesJar
    }
  }

}
