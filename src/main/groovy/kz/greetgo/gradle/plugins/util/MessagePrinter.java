package kz.greetgo.gradle.plugins.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.Nonnull;
import kz.greetgo.gradle.plugins.model.files.MavenConfigFiles;
import kz.greetgo.gradle.plugins.model.files.MybpmConfigFiles;
import kz.greetgo.gradle.plugins.model.files.SignConfigFiles;
import org.gradle.api.Project;

import static java.util.stream.Collectors.joining;
import static kz.greetgo.gradle.plugins.launchers.GreetgoPublisher.UPLOAD_INFO;
import static kz.greetgo.gradle.plugins.model.files.CommonFiles.UPLOAD_TO_MAVEN_REPO;
import static kz.greetgo.gradle.plugins.model.files.CommonFiles.UPLOAD_TO_MYBPM_REPO;

public class MessagePrinter {
  public Project project;

  private final ConcurrentHashMap<String, String> printedMethods = new ConcurrentHashMap<>();

  private boolean wasPrint(String methodName) {
    return printedMethods.putIfAbsent(methodName, methodName) != null;
  }

  public void printNoSonatypeCredentials() {
    if (wasPrint("printNoSonatypeCredentials")) {
      return;
    }

    final MavenConfigFiles f = MavenConfigFiles.get();

    List<String> list = new ArrayList<>();

    list.add("");
    list.add("   No target `" + UPLOAD_TO_MAVEN_REPO + "`.");
    list.add("");
    list.add("Because no file `" + f.file + "` - it is access configuration to Maven Central Repository.");
    list.add("You can create it by using file `" + f.file_template + "`.");
    list.add("Correct this file and rename it");
    list.add("  ");
    list.add("  mv " + f.file_template + " " + f.file);
    list.add("  ");
    list.add("And then target `" + UPLOAD_TO_MAVEN_REPO + "` will appear");
    list.add("");

    printError(list);
  }

  private void printError(@Nonnull List<String> list) {
    Project project = this.project;
    if (project != null) {
      project.getLogger().error(formMessage(list));
    }
  }

  private String formMessage(@Nonnull List<String> list) {
    String left = "** ";

    String s = "*".repeat(6 + list.stream().mapToInt(String::length).max().orElse(1));

    return list.stream().map(it -> left + it).collect(joining("\n", s + "\n", "\n" + s));
  }

  public void printNoMybpmRepoError() {
    if (wasPrint("printNoMybpmRepoError")) {
      return;
    }

    MybpmConfigFiles conf = MybpmConfigFiles.get();

    List<String> list = new ArrayList<>();

    list.add("No file `" + conf.file + "`.");
    list.add("This means no target `" + UPLOAD_TO_MYBPM_REPO + "`.");
    list.add("To add this target edit file `" + conf.template_file + "`.");
    list.add("Correct it to define credentials.");
    list.add("And rename it to `" + conf.file + "`.");
    list.add("  mv " + conf.template_file + " " + conf.file);
    list.add("And then target `" + UPLOAD_TO_MYBPM_REPO + "` will appear.");

    printError(list);
  }

  public @Nonnull RuntimeException throwExtUploadToMavenCentral(String placeId, String errorMessage) {
    return throwError(placeId, errorMessage, linesToPrint_ExtUploadToMavenCentral());
  }

  private @Nonnull RuntimeException throwError(String placeId, String errorMessage, List<String> lines) {
    return new RuntimeException(placeId + " :: " + errorMessage + "\n" + formMessage(lines));
  }

  private @Nonnull List<String> linesToPrint_ExtUploadToMavenCentral() {
    List<String> list = new ArrayList<>();

    list.add("You need to define block '" + UPLOAD_INFO + "' in build.gradle");
    list.add("");
    list.add("For example (short variant):");
    list.add("");
    list.add("  " + UPLOAD_INFO + " {");
    list.add("    description = 'Description of this module: it will appear in MavenCentral'");
    list.add("    url         = 'https://github.com/greetgo/test_project'");
    list.add("  }");
    list.add("");
    list.add("For example (middle variant):");
    list.add("");
    list.add("  " + UPLOAD_INFO + " {");
    list.add("    description = 'Description of this module: it will appear in MavenCentral'");
    list.add("    url         = 'https://tech.greetgo.kz/test_project.php'");
    list.add("    scm {");
    list.add("      url = 'https://github.com/greetgo/test_project'");
    list.add("    }");
    list.add("    developer {");
    list.add("      id    = 'devId'");
    list.add("      name  = 'devName'");
    list.add("      email = 'devEmail@host.kz'");
    list.add("    }");
    list.add("  }");
    list.add("");
    list.add("For example (full variant):");
    list.add("");
    list.add("  " + UPLOAD_INFO + " {");
    list.add("    description = 'Description of this module: it will appear in MavenCentral'");
    list.add("    url         = 'https://tech.greetgo.kz/test_project.php'");
    list.add("    scm {");
    list.add("      url           = 'https://github.com/greetgo/test_project'");
    list.add("      connection    = 'scm:git:https://github.com/greetgo/test_project'");
    list.add("      devConnection = 'scm:git:https://github.com/greetgo/test_project'");
    list.add("    }");
    list.add("    developer {");
    list.add("      id    = 'devId'");
    list.add("      name  = 'devName'");
    list.add("      email = 'devEmail@host.kz'");
    list.add("    }");
    list.add("    developer {");
    list.add("      id    = 'dev2Id'");
    list.add("      name  = 'dev2Name'");
    list.add("      email = 'dev2Email@host.kz'");
    list.add("    }");
    list.add("    //or more developers");
    list.add("  }");
    list.add("");

    return list;
  }

  public void printNoSignConfError() {
    if (wasPrint("printNoSignConfError")) {
      return;
    }

    final SignConfigFiles f = SignConfigFiles.get();

    List<String> list = new ArrayList<>();

    list.add("No signing configuration. Upload to maven will fail.");
    list.add("To sign generating project artifacts you can edit file `" + f.file_template + "`.");
    list.add("Place there correct parameters for your account in Maven Central Repository.");
    list.add("Then rename this file:");
    list.add("  ");
    list.add("  mv " + f.file_template + " " + f.file);
    list.add("  ");

    printError(list);
  }
}
