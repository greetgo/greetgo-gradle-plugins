package kz.greetgo.gradle.plugins.model.files;

import java.nio.file.Path;
import javax.annotation.Nonnull;

public class SignConfigFiles {

  public Path file;
  public Path file_template;

  public static @Nonnull SignConfigFiles get() {

    SignConfigFiles ret = new SignConfigFiles();

    Path root = CommonFiles.configRoot();

    String conf_name = "sign_artifacts";

    ret.file = root.resolve(conf_name);

    String conf_template_name = conf_name + "__template";

    ret.file_template = root.resolve(conf_template_name);

    return ret;
  }
}
