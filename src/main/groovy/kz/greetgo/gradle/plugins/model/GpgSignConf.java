package kz.greetgo.gradle.plugins.model;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import kz.greetgo.gradle.plugins.model.files.SignConfigFiles;
import kz.greetgo.gradle.plugins.util.FileUtil;

public class GpgSignConf extends ConfParent {

  public Path   key_file_location;
  public String key_password;
  public String key_id;

  private static final String KEY_FILE_LOCATION = "key_file_location";
  private static final String KEY_PASSWORD      = "key_password";
  private static final String KEY_ID            = "key_id";

  public static @Nullable GpgSignConf load() throws Exception {

    final SignConfigFiles f = SignConfigFiles.get();

    if (Files.exists(f.file)) {
      GpgSignConf ret = new GpgSignConf();

      Map<String, String> params = FileUtil.configFileToMap(f.file);
      ret.key_id       = params.get(KEY_ID);
      ret.key_password = params.get(KEY_PASSWORD);

      final String signGpgKeyLocation = params.get(KEY_FILE_LOCATION);
      if (signGpgKeyLocation == null || signGpgKeyLocation.isBlank()) {
        throw noParameterErr("6X6Y7Rf1w9", KEY_FILE_LOCATION, f.file);
      }
      ret.key_file_location = Paths.get(signGpgKeyLocation);

      if (!Files.exists(ret.key_file_location)) {
        throw new RuntimeException("v4i3uKbyJw :: File `" + f.file + "` has parameter `" + KEY_FILE_LOCATION + "`" +
                                     "\nwitch is must be path to file with GPG Key File." +
                                     "\nBut there is NO any file by path `" + signGpgKeyLocation + "`");
      }

      if (ret.key_id == null || ret.key_id.isEmpty()) {
        throw noParameterErr("mG5f5I509t", KEY_ID, f.file);
      }
      if (ret.key_password == null || ret.key_password.isEmpty()) {
        throw noParameterErr("W1oEHvF5GJ", KEY_PASSWORD, f.file);
      }

      return ret;
    }

    if (!Files.exists(f.file_template)) {

      List<String> lines = new ArrayList<>();

      lines.add("#");
      lines.add("# Configuration file sign artifacts (for example for Central Maven Repository");
      lines.add("#");
      lines.add("");
      lines.add("# Location of GPG keys to use for signing artifacts");
      lines.add(KEY_FILE_LOCATION + " = /path/to/gpg/key/location");
      lines.add("");
      lines.add("# GPG key id in file with GPG Keys");
      lines.add(KEY_ID + " = <Place here key id>");
      lines.add("");
      lines.add("# Password to access to GPG key id");
      lines.add(KEY_PASSWORD + " = <Place here key password>");
      lines.add("");

      f.file_template.toFile().getParentFile().mkdirs();
      Files.writeString(f.file_template, String.join("\n", lines), StandardCharsets.UTF_8);
    }

    return null;
  }

}
