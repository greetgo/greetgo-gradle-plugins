package kz.greetgo.gradle.plugins.model.files;

import java.nio.file.Path;
import javax.annotation.Nonnull;

public class MybpmConfigFiles {

  public Path file;
  public Path template_file;

  public static @Nonnull MybpmConfigFiles get() {

    var ret = new MybpmConfigFiles();

    Path root = CommonFiles.configRoot();

    String conf_name = "auth_r_mybpm_kz";

    ret.file = root.resolve(conf_name);

    String conf_template_name = conf_name + "__template";

    ret.template_file = root.resolve(conf_template_name);

    return ret;
  }

}
