package plugins

import org.gradle.api.Project
import org.gradle.api.tasks.bundling.Jar

import java.util.concurrent.atomic.AtomicBoolean
import java.util.stream.Collectors

class LocalUtil {

  private static void printListToStderr(List<String> list) {
    def left = "*** "

    def s = "*" * (2 * left.length() + list.stream().mapToInt { it -> it.length() }.max().orElseThrow())

    def message = list.stream().map { it -> left + it }.collect(Collectors.joining("\n", s + "\n", "\n" + s))

    System.err.println(message)
  }

  static void registerSourcesJavadocJarTasks(Project project) {
    project.pluginManager.apply("java")

    def sourcesJar = project.tasks.findByName("sourcesJar")
    if (sourcesJar == null) {
      sourcesJar = project.tasks.create("sourcesJar", Jar.class) {
        group = "documentation"
        from project.sourceSets.main.allSource
        archiveClassifier = 'sources'
      }
    }

    def javadocJar = project.tasks.findByName("javadocJar")
    if (javadocJar == null) {
      javadocJar = project.tasks.create("javadocJar", Jar.class) {
        group = "documentation"
        from project.tasks.javadoc
        //noinspection GroovyAssignabilityCheck
        archiveClassifier = 'javadoc'
      }
    }

    project.artifacts {
      archives javadocJar, sourcesJar
    }
  }

  private static final AtomicBoolean printNoSignEnvHappened = new AtomicBoolean(false)

  static def printNoSignEnv() {
    if (printNoSignEnvHappened.get()) return
    printNoSignEnvHappened.set(true)

    def list = new ArrayList<String>()

    list += "You ask to sign artifacts, but no data to sign"
    list += "Any of needed environment variables do not define"
    list += ""
    list += "You need define the following environment variables:"
    list += ""
    list += "  ${Env.LIB_SIGN_GPG_KEY_ID}       - GPG key sign id"
    list += "  ${Env.LIB_SIGN_GPG_KEY_PASSWORD} - GPG key sign password"
    list += "  ${Env.LIB_SIGN_GPG_KEY_LOCATION} - Location of file with GPG key"
    list += ""
    list += "You can define its in file ~/.pam_environment"

    printListToStderr(list)
  }

}
