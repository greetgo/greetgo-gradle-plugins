package plugins.model.files;

import java.nio.file.Path;
import javax.annotation.Nonnull;

public class MavenConfigFiles {

  public Path file;
  public Path file_template;

  public static @Nonnull MavenConfigFiles get() {

    MavenConfigFiles ret = new MavenConfigFiles();

    Path root = CommonFiles.configRoot();

    String conf_name = "auth_maven_central";

    ret.file = root.resolve(conf_name);

    String conf_template_name = conf_name + "__template";

    ret.file_template = root.resolve(conf_template_name);

    return ret;
  }
}
