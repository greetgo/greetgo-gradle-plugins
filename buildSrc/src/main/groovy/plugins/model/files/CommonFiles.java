package plugins.model.files;

import java.nio.file.Path;
import java.nio.file.Paths;
import javax.annotation.Nonnull;

public class CommonFiles {
  public static final String UPLOAD_TO_MYBPM_REPO = "upload_to_mybpm_repo";
  public static final String UPLOAD_TO_MAVEN_REPO = "upload_to_maven_repo";
  public static final String UPLOAD_TO_LOCAL      = "upload_to_local";

  public static @Nonnull Path configRoot() {
    Path home = Paths.get(System.getProperty("user.home"));

    return home.resolve(".config").resolve("mybpm");
  }
}
