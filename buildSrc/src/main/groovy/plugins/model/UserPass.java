package plugins.model;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import plugins.model.files.MybpmConfigFiles;

public class UserPass {
  public String username;
  public String password;

  public static @Nullable UserPass loadMybpmUserPass() throws Exception {
    MybpmConfigFiles conf = MybpmConfigFiles.get();

    if (Files.exists(conf.file)) {
      UserPass user_pass = new UserPass();

      Map<String, String> params = util.FileUtil.configFileToMap(conf.file);
      user_pass.username = params.get("username");
      user_pass.password = params.get("password");

      if (user_pass.username == null) {
        throw new RuntimeException("n1nwZyP060 :: No `username` in file " + conf.file);
      }

      if (user_pass.password == null) {
        throw new RuntimeException("RLZ7JNeB4X :: No `password` in file " + conf.file);
      }

      return user_pass;
    }

    if (!Files.exists(conf.template_file)) {
      List<String> lines = new ArrayList<>();
      lines.add("");
      lines.add("# This file contains credentials to upload artifacts to repository:");
      lines.add("# ");
      lines.add("#    https://r.mybpm.kz/repository/jar-upload");
      lines.add("# ");
      lines.add("");
      lines.add("");
      lines.add("# username of account to upload artifacts to Nexus Repository on http://nexus.greetgo:8081");
      lines.add("username = <place here username>");
      lines.add("");
      lines.add("# password for username");
      lines.add("password = <place here password>");
      lines.add("");

      String text = String.join("\n", lines);

      conf.template_file.toFile().getParentFile().mkdirs();
      Files.writeString(conf.template_file, text, StandardCharsets.UTF_8);
    }

    return null;
  }

}
