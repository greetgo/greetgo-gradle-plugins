package plugins.model;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import plugins.model.files.MavenConfigFiles;

public class MavenRepoAccess extends ConfParent {

  public String repo_username;
  public String repo_password;

  private static final String USERNAME = "username";
  private static final String PASSWORD = "password";

  public static @Nullable MavenRepoAccess load() throws Exception {

    final MavenConfigFiles f = MavenConfigFiles.get();

    if (Files.exists(f.file)) {
      MavenRepoAccess ret = new MavenRepoAccess();

      Map<String, String> params = util.FileUtil.configFileToMap(f.file);
      ret.repo_username = params.get(USERNAME);
      ret.repo_password = params.get(PASSWORD);

      if (ret.repo_username == null || ret.repo_username.isEmpty()) {
        throw noParameterErr("pjr1vCCR37", USERNAME, f.file);
      }
      if (ret.repo_password == null || ret.repo_password.isEmpty()) {
        throw noParameterErr("7O0317w4wM", PASSWORD, f.file);
      }

      return ret;
    }

    if (!Files.exists(f.file_template)) {

      List<String> lines = new ArrayList<>();

      lines.add("#");
      lines.add("# Configuration file to upload artifacts to Maven Central Repository");
      lines.add("#");
      lines.add("");
      lines.add("# User name of account in Central Maven Repository");
      lines.add(USERNAME + " = <Place here your account name>");
      lines.add("");
      lines.add("# Password of this account");
      lines.add(PASSWORD + " = <Place here your account password>");
      lines.add("");

      f.file_template.toFile().getParentFile().mkdirs();
      Files.writeString(f.file_template, String.join("\n", lines), StandardCharsets.UTF_8);
    }

    return null;
  }

}
