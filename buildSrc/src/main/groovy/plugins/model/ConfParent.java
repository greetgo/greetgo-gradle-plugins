package plugins.model;

import java.nio.file.Path;
import javax.annotation.Nonnull;

public class ConfParent {

  protected static @Nonnull RuntimeException noParameterErr(String placeId, String parameterName, Path file) {
    return new RuntimeException(placeId + " :: No parameter `" + parameterName + "` in file `" + file + "`");
  }

}
