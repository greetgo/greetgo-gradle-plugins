package plugins

import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.configuration.project.ProjectConfigurationActionContainer
import org.gradle.plugins.signing.Sign
import plugins.model.*
import plugins.model.files.CommonFiles

import javax.inject.Inject

class GreetgoPublisher implements Plugin<Project> {
  public static final String UPLOAD_INFO = "uploadInfo"

  private final ProjectConfigurationActionContainer configurationActionContainer

  @Inject
  GreetgoPublisher(ProjectConfigurationActionContainer configurationActionContainer) {
    this.configurationActionContainer = configurationActionContainer
  }

  @Override
  void apply(Project project) {

    MessagePrinter messagePrinter = new MessagePrinter(project)

    MavenRepoAccess mavenRepoAccess = MavenRepoAccess.load()
    if (mavenRepoAccess == null) {
      messagePrinter.printNoSonatypeCredentials()
    }

    UserPass userPass = UserPass.loadMybpmUserPass()
    if (userPass == null) {
      messagePrinter.printNoMybpmRepoError()
    }

    MavenUploadPluginExtension ext = project.extensions.create(UPLOAD_INFO, MavenUploadPluginExtension)

    LocalUtil.registerSourcesJavadocJarTasks(project)

    GpgSignConf signConf = GpgSignConf.load()
    if (signConf == null) {
      messagePrinter.printNoSignConfError()
    }

    project.pluginManager.apply("java")
    project.pluginManager.apply("maven-publish")

    if (signConf != null) {
      project.pluginManager.apply("signing")

      project.gradle.taskGraph.whenReady { taskGraph ->
        if (taskGraph.allTasks.any { it instanceof Sign }) {
          project.gradle.allprojects { it.ext."signing.secretKeyRingFile" = signConf.key_file_location }
          project.gradle.allprojects { it.ext."signing.password" = signConf.key_password }
          project.gradle.allprojects { it.ext."signing.keyId" = signConf.key_id }
        }
      }

    }

    configurationActionContainer.add {

      if (ext.description == null) {
        throw messagePrinter.throwExtUploadToMavenCentral("klr96bhtO7", UPLOAD_INFO + ".description == null")
      }
      if (ext.url == null) {
        throw messagePrinter.throwExtUploadToMavenCentral("klr96bhtO7", UPLOAD_INFO + ".url == null")
      }

      project.publishing {
        publications {
          mainJava(MavenPublication) {
            from project.components.java

            pom {
              //noinspection GroovyAssignabilityCheck
              packaging = "jar"
              name.set(project.name)

              description.set(ext.description)
              url.set(ext.url)

              scm {
                connection.set(ext.scm.url)
                developerConnection.set(ext.scm.connection)
                url.set(ext.scm.devConnection)
              }

              licenses {
                license {
                  name.set("The Apache License, Version 2.0")
                  url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                }
              }

              developers {
                for (int i = 0; i < ext.developers.size(); i++) {
                  DeveloperModel d = ext.developers[i]
                  developer {
                    id.set(d.id)
                    name.set(d.name)
                    email.set(d.email)
                  }
                }
              }
            }
          }
        }

        repositories {

          if (mavenRepoAccess != null) {

            maven {
              name = "maven"
              def releasesUrl = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2/")
              def snapshotsUrl = uri("https://oss.sonatype.org/content/repositories/snapshots/")
              url = version.toString().endsWith("SNAPSHOT") ? snapshotsUrl : releasesUrl
              credentials {
                username = mavenRepoAccess.repo_username
                password = mavenRepoAccess.repo_password

                // username = Env.sonatypeAccountId()
                // password = Env.sonatypeAccountPassword()
              }
            }

          }

          if (userPass != null) {

            maven {
              allowInsecureProtocol = true
              name = "mybpm"
              url = uri("https://r.mybpm.kz/repository/jar-upload/")

              if (userPass != null) {
                credentials {
                  username = userPass.username
                  password = userPass.password
                  //allowInsecureProtocol = true
                }
              }
            }

          }
        }

      }

      if (signConf != null) {
        project.signing {
          sign publishing.publications['mainJava']
        }
      }

      project.java {
        withJavadocJar()
        withSourcesJar()
      }

      project.tasks.withType(Javadoc) {
        //noinspection GroovyAssignabilityCheck
        failOnError false
        //noinspection SpellCheckingInspection
        options.addStringOption('Xdoclint:none', '-quiet')
        options.addStringOption('encoding', 'UTF-8')
        options.addStringOption('charSet', 'UTF-8')
      }

      if (userPass != null) {
        project.tasks.create(CommonFiles.UPLOAD_TO_MYBPM_REPO, DefaultTask.class) {
          group = "upload"
          dependsOn "publishMainJavaPublicationToMybpmRepository"
        }
      }

      if (mavenRepoAccess != null) {
        project.tasks.create(CommonFiles.UPLOAD_TO_MAVEN_REPO, DefaultTask.class) {
          group = "upload"
          dependsOn "publishMainJavaPublicationToMavenRepository"
        }
      }

      project.tasks.create(CommonFiles.UPLOAD_TO_LOCAL, DefaultTask.class) {
        group = "upload"
        dependsOn "publishMainJavaPublicationToMavenLocal"
      }

    }

  }
}
